package gnukhata.controllers;

/**
 * @author girish
 *
 */

import java.awt.print.Book;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import gnukhata.globals;
import gnukhata.controllers.reportmodels.ProjectList;
import gnukhata.views.AddMoreProjects;
import gnukhata.views.CreateAccountForm;
import gnukhata.views.InitialSetupForm;
import gnukhata.views.LoginForm;
import gnukhata.views.MainShell;
import gnukhata.views.PreferencesForm;
import gnukhata.views.startupForm;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClientException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;


/*
 * The <code>Startup</code>class contains all the methods for xmlrpc connection required for the startup of gnukhata.
 * 
 */
public class StartupController
{
	/*
	 * @code getFinancialyear returns an array of the object containing the
	 * financial years of the given organisation.
	 * 
	 * @param Vector name of the organisation. returns @code Object[] in which
	 * each element is an array of two objects, first is starting date of the
	 * financial year and another is the ending date of the organisation.
	 */

	public static Object[] getFinancialYear(java.util.Vector<Object> params) {
		Object[] result;

		try
		{
			result = (Object[]) gnukhata.globals.client.execute("getFinancialYear", params);
			return result;
		} catch (XmlRpcException e)
		{
			// TODO Auto-generated catch block
			e.getMessage();
			return new Object[] {};
		}
	}

	/*
	 * @return the @code String[] containing the names of the organisations.
	 */

	public static String[] getOrganisationNames() {

		try
		{

			System.out.println("getorgnames");
			// only execute this once for setting the connection.
			Object[] params = new Object[] {};
			Object[] result = (Object[]) gnukhata.globals.client.execute("getOrganisationNames", params);

			System.out.println("result is" + result);
			String[] names = new String[result.length +1];
			names[0] = "--select--";
			// this.finyear[]=new Strng ();
			for (int i = 0; i < result.length; i++)
			{
				System.out.println("result[ " + i + "] is " + result[i]);
				names[i +1] = result[i].toString();
			}
			return names;
		} catch (XmlRpcException exc)
		{
			exc.printStackTrace();

		}
		catch (NullPointerException e) {
			// TODO: handle exception
			e.printStackTrace();
		
		}
		return new String[] { "", "" };
	}

	/*
	 * @return the client_id and sets the data in the @code session in
	 * gnukhata.globals.session.
	 */
	public static String[] getOrgDetail()
	{
		
		ArrayList<Object> serverParams = new ArrayList<Object>();
		//serverParams.add(new Object[]{} );
		serverParams.add(globals.session[0]);
		
		try {
			Object[] orgDetails = (Object[]) globals.client.execute("organisation.getOrgDetails",serverParams);
			String[] details = new String[orgDetails.length ];
			for(int counter =0; counter < orgDetails.length; counter ++  )
			{
				details[counter] = orgDetails[counter].toString();
			}			
			return details;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return new String[]{};
		}
	}
	
	public static boolean updateOrgDetail(ArrayList<String> paramsOrg)
	{
		ArrayList<Object> serverParams = new ArrayList<Object>();
		serverParams.add(paramsOrg);
		serverParams.add(globals.session[0]);
		try {
			Object success = globals.client.execute("organisation.updateOrgDetails",serverParams);
			return true;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
	public static boolean duplicateOrganisation(String orgName,String fromdate,String enddate)
	{
		ArrayList<Object> serverParams = new ArrayList<Object>();
		serverParams.add(orgName);
		serverParams.add(fromdate);
		serverParams.add(enddate);
		try {
			Object success = globals.client.execute("organisation.getDuplicateOrg",serverParams);
			return true;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	public static String getConnection(String[] params) {
		try
		{
			// call this only once to initialise the proper url in client
			// connection.

			String result = (String) gnukhata.globals.client.execute("getConnection", new Object[] { params });

			globals.session[0] = result;
			globals.session[1] = params[0];
			globals.session[2] = params[1];
			globals.session[3] = params[2];
			globals.session[4] = "";
			globals.session[5] = "";

			return result;
		} catch (XmlRpcException exc)
		{
			exc.printStackTrace();
			return null;
		} catch (NullPointerException e)
		{
			// TODO: handle exception
			return null;
		}
		
	}

	
	public static boolean deploy(Vector<String> deployParams)
	{
		//take the orgname, fromdate, todate and orgtype respectively and pass it on to rpc_deploy.
		try
		{
			Object[] result = (Object[]) gnukhata.globals.client.execute("Deploy", new Object[]{deployParams});
			globals.session[0] = result[1];
			globals.session[1] = deployParams.get(0);
			globals.session[2] = deployParams.get(1);
			globals.session[3] = deployParams.get(2);
			globals.session[4] = deployParams.get(3);
			Vector<Object> serverParams = new Vector<Object>();
			serverParams.add(new String[]{"","","","","","","","","","","","","","","","","","",""} );
			serverParams.add(globals.session[0]);
			globals.client.execute("organisation.setOrganisation", serverParams);
			
			System.out.println("now showing the preferences");
			return true;
			//gnukhata.views.PreferencesForm preferences = new PreferencesForm(); 
		}
		catch(XmlRpcException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	public static boolean deploy(Vector<String> deployParams,Vector<String> orgParams)
	{
		try
		{
			Object[] result = (Object[]) gnukhata.globals.client.execute("Deploy", new Object[]{deployParams});
			globals.session[0] = result[1];
			globals.session[1] = deployParams.get(0);
			globals.session[2] = deployParams.get(1);
			globals.session[3] = deployParams.get(2);
			globals.session[4] = deployParams.get(3);
			Vector<Object> serverParams = new Vector<Object>();
			serverParams.add(orgParams);
			serverParams.add(globals.session[0]);
			globals.client.execute("organisation.setOrganisation", serverParams);
			return true;
			
			
		}
		catch(XmlRpcException e)
		{
			e.printStackTrace();
			return false;
		
		}

	}
	public static void showInitialSetup(String[] initialParams)
	{
		//render the initial setup form which user uses to enter details of organization or skip.
		gnukhata.views.InitialSetupForm initialSetup = new InitialSetupForm(initialParams);
		initialSetup.pack();
		initialSetup.open();
		}
	public static void showLoginForm()
	{
		LoginForm lf = new LoginForm();
		lf.pack();
		lf.open();	
	}
	
	public static void showstartupForm()
	{
		startupForm sf = new startupForm();
		sf.pack();
		sf.open();	
	}
	
	public static boolean setProjects(HashMap<String,String> projectParams) 
	{
		//Vector<Object> projectAmount = new Vector<Object>();
		Object[] projectAmount = new Object[2];
		boolean returnFlag = true; 
		Iterator<String> keyItr = projectParams.keySet().iterator();
		Iterator<String> valueItr = projectParams.values().iterator();

		while (keyItr.hasNext())
		{
			projectAmount[0]=keyItr.next();
			projectAmount[1]=valueItr.next().toString();
			System.out.println(projectAmount[0].toString()+ " " + projectAmount[1].toString());
			try
			{
				List<Object> lstParams = new ArrayList<Object>();
				lstParams.add(projectAmount);
				lstParams.add(globals.session[0]);
				Object result = globals.client.execute("organisation.setProjects", lstParams );

				returnFlag = true; 
			}
			catch(Exception e)
			{
				e.printStackTrace();
				returnFlag = false;
			}
			
			//projectAmount.removeAllElements();
		}
		return returnFlag;
	}
	
	/*
	 * @param session is the session returned by getConnection() method.
	 * 
	 * @param login_details array of Object (username,password).
	 * 
	 * @return the login status which is an array of object with two elements.
	 * first is boolean and another is an integer.
	 */
	//this function is for setting the preferences.
	//note that we just set the account code to manually or automatic.
	public static boolean changePasswordOwn(String username, String oldpassword, String newpassword)
	{
		List<Object> changeparams = new ArrayList<Object>();
		changeparams.add(new Object[]{username,oldpassword,newpassword} );
		changeparams.add(globals.session[0]);
		try
		{
			Object result = globals.client.execute("user.changePassword",changeparams);
			return Boolean.valueOf(result.toString() );
			//return true;
		}
		catch(Exception e)
		{
			e.getMessage();
			return false;
		}
	}
	
	public static boolean changePasswordOthers(String username,String newpassword)
	{
		List<Object> changeparams = new ArrayList<Object>();
		changeparams.add(new Object[]{username,newpassword} );
		changeparams.add(globals.session[0]);
		try
		{
			Object result = globals.client.execute("user.changePassword",changeparams);
			return Boolean.valueOf(result.toString() );
			//return true;
		}
		catch(Exception e)
		{
			e.getMessage();
			return false;
		}
	}
	
	public static boolean CloseBooks(String financialstart,String fromdate,String enddate)
	{
		List<Object> closeparams = new ArrayList<Object>();
		closeparams.add(new Object[]{financialstart,fromdate,enddate} );
		closeparams.add(globals.session[0]);
		try
		{
			Object roResult = globals.client.execute("getRollOverStatus", new Object[]{globals.session[0]} );
			int roStatus = Integer.parseInt(roResult.toString());
			if(roStatus ==0)
			{
				Object result = globals.client.execute("closeBooks",closeparams);
				/*gnukhata.controllers.StartupController.showstartupForm();*/
				/*startupForm sf=new startupForm();
				sf.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);*/
				Vector<Object> roParams = new Vector<Object>();
				roParams.add(new Object[]{1});
				roParams.add(globals.session[0]);
				Object setRo = globals.client.execute("setRollOverStatus", roParams );
				return true;

			}
			else
			{
				return false;
			}
		}
		catch(Exception e)
		{
			e.getMessage();
			return false;
		}
	}

	public static boolean RollOver(String orgname,String  fromdate,String todate,String enddate,String type)
	{
		List<Object> rollparams = new ArrayList<Object>();
		rollparams.add(new Object[]{orgname,fromdate,todate,enddate,type} );
		rollparams.add(globals.session[0]);

		try
		{
			Object cbResult = globals.client.execute("getRollOverStatus", new Object[]{globals.session[0]});
			int closedBooksStatus  = Integer.parseInt(cbResult.toString());
			if(closedBooksStatus== 1 )
			{
				Object rolledResult = globals.client.execute("getClosedBooksStatus",new Object[]{globals.session[0]} );
				System.out.println("closed result is " + rolledResult.toString());
				int cbStatus = Integer.parseInt(rolledResult.toString());
				if(cbStatus ==0)
				{
					System.out.println("about to rollover");
					Vector<Object> setcbStatusParam = new Vector<Object>();
					setcbStatusParam.add(new Object[]{1} );
					setcbStatusParam.add(globals.session[0]);
					Object cbSetResult = globals.client.execute("setClosedBooksStatus", setcbStatusParam);
					System.out.println("closedbooksstatus is now set to 1");
					Object result = globals.client.execute("rollover",rollparams);
					System.out.println("rollover done");

					return true;
				}
				else
				{
					return false;
				}

				

			}
			else
			{
				return false;
			}
		}
		catch(Exception e)
		{
			e.getMessage();
			return false;
		}
	}
	
	public static boolean getClosedBooksStatus()
	{
			try {
				Object cbResult = globals.client.execute("getClosedBooksStatus", new Object[]{globals.session[0]} );
				int closedbooksStatus = Integer.parseInt(cbResult.toString());
				if(closedbooksStatus ==0)
				{
					return true;
				}
				else
				{
					return false;
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}

		}
	
	public static boolean getRolloverStatus()
	{
			try {
				Object roResult = globals.client.execute("getRollOverStatus", new Object[]{globals.session[0]} );
				int roStatus = Integer.parseInt(roResult.toString());
				if(roStatus ==0)
				{
					return true;
				}
				else
				{
					return false;
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}

		}

	
	public static boolean DeleteOrg(Composite grandParent,String Orgname,String financialstart,String financialend)
	{
		List<Object> delorgparams = new ArrayList<Object>();
		delorgparams.add(new Object[]{Orgname,financialstart,financialend} );
		delorgparams.add(globals.session[0]);
		try
		{
			Object result = globals.client.execute("deleteOrganisation",delorgparams);
			/*gnukhata.controllers.StartupController.showstartupForm();*/
			/*startupForm sf=new startupForm();
			sf.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);*/
			return true;
		}
		catch(Exception e)
		{
			e.getMessage();
			return false;
		}
	}
	public static boolean createuser(String username,String password,int userrole)
	{
		List<Object> createuserparams = new ArrayList<Object>();
		createuserparams.add(new Object[]{username,password,userrole} );
		createuserparams.add(globals.session[0]);
		try
		{
			Object result = globals.client.execute("user.setUser",createuserparams);
			return true;
		}
		catch(Exception e)
		{
			e.getMessage();
			return false;
		}
	}
	
	
	
	public static boolean userExists(String username)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{username} );
		serverParams.add(globals.session[0]);
		try {
			Object result = globals.client.execute("user.isUserUnique",serverParams);
			System.out.println(result.toString());
			return Boolean.valueOf(result.toString() );
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return false;
		}
	}
	public static boolean setPreferences(String accountCodeFlag)
	{
		List<Object> prefParams = new ArrayList<Object>();
		prefParams.add(new Object[]{2,accountCodeFlag} );
		prefParams.add(globals.session[0]);
		try
		{
			Object result = globals.client.execute("organisation.setPreferences",prefParams);
			return true;
		}
		catch(Exception e)
		{
			e.getMessage();
			return false;
		}
	}
	
	public static void showCreateAccount()
	{	
		gnukhata.views.CreateAccountForm createAccount = new CreateAccountForm();
		createAccount.pack();
		createAccount.open();
		
	}
	public static boolean login(String userName, String password) {
		try
		{
			System.out.println("inside first try");
			List<Object> serverParams = new ArrayList<Object>();
			serverParams.add(new Object[]{userName,password});
			serverParams.add(globals.session[0]);
			
			

			Object loginSuccess =  gnukhata.globals.client.execute("user.getUser",serverParams);
			System.out.println("result is " +loginSuccess.toString());
			
			try {
/*				if ( new Boolean(loginSuccess.toString()) == false)
				{
					return false;
				}
*/
				Object[] userParams = (Object[]) loginSuccess;
				System.out.println(userParams[0].toString()+ " " + userParams[1].toString());
				globals.session[6] = userParams[0];
				globals.session[7] = userParams[1];

				return true;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.getMessage();
				return false;
			
			}
		} catch (XmlRpcException exc)
		{
			exc.printStackTrace();
			return false;
		} 
		
	}
	public static void showMainShell(Display display,int FlagNo)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{FlagNo} );
		serverParams.add(globals.session[0]);
		//System.out.println(globals.session[0].toString() );
		
		try {
			Object result = globals.client.execute("organisation.getPreferences",serverParams);
			System.out.println(result.toString());
			globals.session[5] = result.toString();
			serverParams.clear();
			Object orgType = globals.client.execute("organisation.getOrganizationType",new Object[]{globals.session[0]});
			if(Boolean.valueOf(orgType.toString()))
			{
				globals.session[4] = "ngo";
			}
			else
			{
				globals.session[4] = "profit making";
			}
			System.out.println(globals.session[4]);
			
			MainShell  mf = new MainShell(display);
			mf.pack();
			mf.open();
			
			} 
		catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			}
	}
	/*public static Object getPreferances(int FlagNo)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(FlagNo);
		serverParams.add(globals.session[0]);
		try {
			Object result = globals.client.execute("organisation.getPreferances",serverParams);
			System.out.println(result.toString());
			globals.session[5] = result.toString(); 
			return result;
			} 
		catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return null;
			}
		
		
	}*/

	/*
	 * creates new organisation.
	 * 
	 * @param params array of Object containing organisation details
	 * 1.organisation name 2.from date 3.to date 4.organisation type
	 * "Profit Making" or "NGO"
	 * 
	 * @param data array of Object containing organisation details to be stored.
	 * 
	 * @param button integer used as a flag. if button =1 the organisation
	 * details are saved to the database. otherwise skipped.
	 */
	
	/*
	 * @param Object the name of the country.
	 * 
	 * @return String[] the names of all the states in that country.
	 */

	public static String[] getStates() {
		Object result[];
		try
		{
			result = (Object[]) gnukhata.globals.client.execute("data.getStateNames",new Object[] {});
			String states[] = new String[result.length];
			for (int i = 0; i < result.length; i++)
			{
				Object[] obj = (Object[]) result[i];
				states[i] = obj[0].toString();
			}
			Arrays.sort(states);
			return states;

		} catch (XmlRpcException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	/*
	 * @param params Object[0] name of the state.
	 * 
	 * @return String[] names of all the cities in that state.
	 */

	public static String[] getCities(String state) {
		try
		{
			ArrayList<Object> serverParams = new ArrayList<Object>();
			serverParams.add(new Object[]{state});
			Object[] result = (Object[]) gnukhata.globals.client.execute("data.getCityNames", serverParams );
			String cities[] = new String[result.length];
			for (int i = 0; i < result.length; i++)
			{
				cities[i] = result[i].toString();
			}

			return cities;
		} catch (XmlRpcException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new String[] { "", "" };
	}

	/*
	 * logout function
	 */
	public static void closeConnection() {

		try
		{

			globals.session[1] = null;
			globals.session[2] = null;

			gnukhata.globals.client.execute("closeConnection", new Object[] { gnukhata.globals.session[0] });
			globals.session[0] = null;
			System.out.println("you have successfully loggedout.");
		} catch (XmlRpcException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public static String[] getProjects()
	{
		ArrayList<Object > serverParams = new ArrayList<Object>();
		serverParams.add(globals.session[0] );
		try {
			Object[] result = (Object[])  globals.client.execute("organization.getAllProjects", serverParams );
			String[] projects = new String[result.length ];
			for(int i = 0; i < result.length; i++)
			{
				Object[] projectrow = (Object[]) result[i];
				projects[i] = projectrow[1].toString();
				
			}
			return projects;
			
			
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new String[]{};
		}
		
	}
	
	public static void getProjectList(Composite grandParent)
	{
		System.out.println("this work 0");
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(globals.session[0]);
		try
		{
			Object[] result=(Object[])globals.client.execute("organisation.getListOfProject",serverParams);
			System.out.println("this work 1");
			ArrayList<ProjectList> prjdata = new ArrayList<ProjectList>();
			System.out.println("this work");
			for (int prjcounter = 0; prjcounter < result.length; prjcounter++)
			{
				Object[] tbRow = (Object[]) result[prjcounter];
				String srNo = tbRow[0].toString();
				System.out.println(srNo);
				String prjname = tbRow[1].toString();
				System.out.println(prjname);
				String prjamount = tbRow[2].toString();
				System.out.println(prjamount);
				ProjectList prjlist= new ProjectList(srNo,prjname, prjamount);
				
				prjdata.add(prjlist);
									
			}
			
			AddMoreProjects amp=new AddMoreProjects(grandParent, SWT.NONE, prjdata);
			amp.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			
		}
		catch (XmlRpcException e)
		{
			e.printStackTrace();
		}
		
	}
	
}
