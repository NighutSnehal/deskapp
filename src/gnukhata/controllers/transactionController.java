package gnukhata.controllers;

import gnukhata.globals;
import gnukhata.views.EditVoucherComposite;
import gnukhata.views.ViewVoucherDetailsComposite;
import gnukhata.views.VoucherTabForm;

import java.util.ArrayList;
import java.util.List;

import org.apache.xmlrpc.XmlRpcException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class transactionController {
	public static String[] getContra()
	{
		try {
			Object[] contraAccounts =(Object[]) globals.client.execute("getaccountsbyrule.getContraAccounts",new Object[]{globals.session[0]});
			String[] accountList = new String[contraAccounts.length];
			for(int i =0; i<contraAccounts.length; i++)
			{
				accountList[i] = contraAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{}; 
		}
				
	}
	public static String[] getJournal()
	{
		try {
			Object[] journalAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getJournalAccounts",new Object[]{globals.session[0]});
			String[] accountList = new String[journalAccounts.length];
			for(int i =0;i <journalAccounts.length;i++)
			{
				accountList[i] = journalAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getPayment(String DrCrFlag)
	{	
		
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);
		
		
		try {
			Object[] paymentAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getPaymentAccounts",serverParams);
			String[] accountList = new String[paymentAccounts.length];
			for(int i =0;i <paymentAccounts.length;i++)
			{
				accountList[i] = paymentAccounts[i].toString();
				System.out.println(paymentAccounts[i]);
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getReceipt(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);

		try {
			Object[] receiptAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getReceivableAccounts",serverParams);
			String[] accountList = new String[receiptAccounts.length];
			for(int i =0;i <receiptAccounts.length;i++)
			{
				accountList[i] = receiptAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getCreditNote(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);
		try {
			Object[] creditNoteAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getCreditNoteAccounts",serverParams);
			String[] accountList = new String[creditNoteAccounts.length];
			for(int i =0;i <creditNoteAccounts.length;i++)
			{
				accountList[i] = creditNoteAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getDebitNote(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);

		try {
			Object[] DebitNoteAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getDebitNoteAccounts",serverParams);
			String[] accountList = new String[DebitNoteAccounts.length];
			for(int i =0;i <DebitNoteAccounts.length;i++)
			{
				accountList[i] = DebitNoteAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getSales(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);

		try {
			Object[] salesAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getSalesAccounts",serverParams);
			String[] accountList = new String[salesAccounts.length];
			for(int i =0;i <salesAccounts.length;i++)
			{
				accountList[i] = salesAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getSalesReturn(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);

		try {
			Object[] salesReturnAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getSalesReturnAccounts",serverParams);
			String[] accountList = new String[salesReturnAccounts.length];
			for(int i =0;i <salesReturnAccounts.length;i++)
			{
				accountList[i] = salesReturnAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getPurchase(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);
		try {
			Object[] purchaseAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getPurchaseAccounts",serverParams);
			String[] accountList = new String[purchaseAccounts.length];
			for(int i =0;i <purchaseAccounts.length;i++)
			{
				accountList[i] = purchaseAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getPurchaseReturn(String DrCrFlag)
	{		
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);
		try {
			Object[] purchaseReturnAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getPurchaseReturnAccounts",serverParams);
			String[] accountList = new String[purchaseReturnAccounts.length];
			for(int i =0;i <purchaseReturnAccounts.length;i++)
			{
				accountList[i] = purchaseReturnAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	
	public static boolean setTransaction(List<String> masterQueryParams, List<Object> detailQueryParams)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(masterQueryParams);
		serverParams.add(detailQueryParams);
		serverParams.add(globals.session[0]);
		try {
			Object result = globals.client.execute("transaction.setTransaction",serverParams);
			return true;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return false;
		}
		
	}
	
	public static boolean editTransaction(List<String> masterQueryParams, List<Object> detailQueryParams )
	{
		ArrayList<Object> serverParams = new ArrayList<Object>();
		serverParams.add(masterQueryParams);
		serverParams.add(detailQueryParams);
		serverParams.add(globals.session[0]);
			try {
				Object success = globals.client.execute("transaction.editVoucher", serverParams);
				return true;
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		
		
	}
	
	public static Object[] searchVouchers(int searchFlag, String txtvoucherno, String startRange, String endRange, String txtnarration, double amount)
	{
		ArrayList<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{searchFlag, txtvoucherno, startRange, endRange, txtnarration, amount});
		serverParams.add(globals.session[0]);
		try {
			Object[] result = (Object[]) globals.client.execute("transaction.searchVoucher",serverParams);
			return result;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new Object[0];
		}
		
		
		
	}
	public static void showVoucherDetail(Composite grandParent, String typeFlag,int voucherCode,boolean findvoucher, boolean tbdrilldown, boolean psdrilldown,String tbType, boolean ledgerDrilldown,String startDate,String oldfromdate, String endDate, String oldenddate,String AccountName,String oldaccname, String ProjectName,String oldprojectname,boolean narrationFlag,boolean oldnarration,String selectproject,String oldselectproject,boolean dualledgerflag)
	{
		ViewVoucherDetailsComposite vvdc = new ViewVoucherDetailsComposite(grandParent, SWT.NONE, typeFlag, voucherCode, false, tbdrilldown, psdrilldown, tbType, ledgerDrilldown, startDate,oldfromdate, endDate,oldenddate, AccountName,oldaccname, ProjectName,oldprojectname, narrationFlag,oldnarration, selectproject,oldselectproject,dualledgerflag);
		vvdc.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height );
	}
public static void showVoucherDetail(Composite grandParent,String typeFlag,int voucherCode,boolean findvoucher)
{
	
	ViewVoucherDetailsComposite vvdc = new ViewVoucherDetailsComposite(grandParent, SWT.NONE, typeFlag, voucherCode, findvoucher, false, false, "", false, "","", "","", "","","", "", false,false, "","",false);
	vvdc.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height );
	
}
public static Object[] getVoucherMaster(int voucherCode)
{
	ArrayList<Object> serverParams = new ArrayList<Object>();
	serverParams.add(new Object[]{voucherCode} );
	serverParams.add(globals.session[0] );
	try {
		Object[] result = (Object[])globals.client.execute("transaction.getVoucherMaster", serverParams);
		return result;
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return new Object[]{};
	}
}
public static Object[] getVoucherDetails(int voucherCode)
{
	ArrayList<Object> serverParams = new ArrayList<Object>();
	serverParams.add(new Object[]{voucherCode} );
	serverParams.add(globals.session[0] );
	try {
		Object[] result = (Object[])globals.client.execute("transaction.getVoucherDetails", serverParams);
		return result;
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return new Object[]{};
	}

}
public static void showEditCloneVoucher(Composite grandparent, int style,String voucherType, int voucherCode,boolean findvoucher, int editFlag, boolean tbdrilldown,boolean psdrilldown,String tbType, boolean ledgerDrilldown, String startDate ,String oldfromdate, String endDate,String oldenddate, String AccountName,String oldaccname, String ProjectName,String oldprojectname, boolean narrationFlag,boolean oldnarration,String selectproject,String oldselectproject,boolean dualledgerflag )
{
	
	EditVoucherComposite evc = new EditVoucherComposite(grandparent, SWT.NONE, voucherType, voucherCode,findvoucher, editFlag, tbdrilldown, psdrilldown, tbType, ledgerDrilldown, startDate,oldfromdate, endDate,oldenddate, AccountName,oldaccname, ProjectName,oldprojectname, narrationFlag,oldnarration, selectproject,oldselectproject,dualledgerflag);
	evc.setSize(grandparent.getClientArea().width, grandparent.getClientArea().height );
}

public static boolean deleteVoucher(int voucherCode)
{
	ArrayList<Object> serverParams = new ArrayList<Object>();
	try {
		serverParams.add(new Object[]{voucherCode});
		serverParams.add(globals.session[0]);
		
		Object success = globals.client.execute("transaction.deleteVoucher", serverParams);
		return true;
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
	}
	
}


public static String getLastDate(String typeFlag)
{
	ArrayList<Object> serverParams = new ArrayList<Object>();
	String fromDateParam = globals.session[2].toString().substring(6) + "-" + globals.session[2].toString().substring(3,5) + "-" + globals.session[2].toString().substring(0,2);
	
	serverParams.add(new Object[]{fromDateParam,typeFlag});
	serverParams.add(globals.session[0]);
	try {
		Object lastDate = globals.client.execute("transaction.getLastReffDate", serverParams);
		return lastDate.toString();
	
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return "";
	}
	
}

public static String getLastReference(String typeFlag)
{
	ArrayList<Object> serverParams=new ArrayList<Object>();
	serverParams.add(new Object[]{typeFlag});
	serverParams.add(globals.session[0]);

	try {
		Object voucherno = globals.client.execute("transaction.getLastReference",serverParams);
		return voucherno.toString();
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return "";
	}
	
	
	}

public static String[] getAllProjects()
{
	try {
		Object[] projects = (Object[]) globals.client.execute("organisation.getAllProjects", new Object[]{globals.session[0]});
		String[] allProjects = new String[projects.length];
		for(int i = 0; i < projects.length; i++ )
		{
			Object[] projectRow = (Object[]) projects[i];
			allProjects[i] = projectRow[1].toString();
		}
		return allProjects;
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return new String[]{};
	}
	
}
}


